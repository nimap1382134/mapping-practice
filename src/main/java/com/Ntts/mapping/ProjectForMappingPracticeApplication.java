package com.Ntts.mapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectForMappingPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectForMappingPracticeApplication.class, args);
	}

}
