package com.Ntts.mapping.entities;

import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import jakarta.persistence.AssociationOverride;
import jakarta.persistence.AssociationOverrides;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.Table;

@AssociationOverrides({@AssociationOverride(name = "pk.users" , joinColumns = @JoinColumn(name = "user_id")),
   @AssociationOverride(name = "pk.roles" , joinColumns = @JoinColumn(name = "role_id")) })
@Entity
@Table(name = "user_role")
public class user_role {
	
	@EmbeddedId
	private UserRoleId1 pk = new UserRoleId1();
	
	@CreationTimestamp
	@Column(name = "created_at")
	private Date createdAt;
	
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Date updatedAt;
	
	@Column(name = "is_active")
	private boolean isActive;
}