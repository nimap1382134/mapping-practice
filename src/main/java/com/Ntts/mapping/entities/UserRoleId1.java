package com.Ntts.mapping.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToOne;

@Embeddable
public class UserRoleId1 {
	
	@JsonBackReference
	@ManyToOne
	private user_entity users;

	@JsonBackReference
	@ManyToOne(fetch = FetchType.EAGER , cascade = CascadeType.ALL)
	private RoleEntity roles;

	public UserRoleId1(user_entity users, RoleEntity roles) {
		super();
		this.users = users;
		this.roles = roles;
	}

	public UserRoleId1() {
		super();
		// TODO Auto-generated constructor stub
	}

	public user_entity getUsers() {
		return users;
	}

	public void setUsers(user_entity users) {
		this.users = users;
	}
@ManyToOne
	public RoleEntity getRoles() {
		return roles;
	}

	public void setRoles(RoleEntity roles) {
		this.roles = roles;
	}
	
	
	

}
