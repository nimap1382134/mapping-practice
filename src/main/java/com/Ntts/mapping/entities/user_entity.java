package com.Ntts.mapping.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

@Entity
@Table(name="users")
public class user_entity implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "user_name")
	@NotEmpty(message = "must me fill")
	private String name;
	
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@Column(name = "email" , unique = true)
	private String email;
	
	@Column(name = "created_at")
	@CreationTimestamp
	private Date CreatedAt;
	
	@Column(name = "updated_at")
	@UpdateTimestamp
	private Date UpdatedAt;

	
	@Column(name = "phone_number")
	@Size(max = 10, min = 10)
	private String phone;
	
	@Column(name = "address1")
	private String Address1;
	
	@Column(name = "address2")
	private String Address2;
	
	@Column(name = "is_active")
	private Boolean isActive = true;

	public user_entity() {
		super();
		// TODO Auto-generated constructor stub
	}
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL , mappedBy = "pk.users")
	@JsonBackReference
	List<user_role> userrole;

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getCreatedAt() {
		return CreatedAt;
	}
	public void setCreatedAt(Date createdAt) {
		CreatedAt = createdAt;
	}
	public Date getUpdatedAt() {
		return UpdatedAt;
	}
	public void setUpdatedAt(Date updatedAt) {
		UpdatedAt = updatedAt;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getAddress1() {
		return Address1;
	}
	public void setAddress1(String address1) {
		Address1 = address1;
	}
	public String getAddress2() {
		return Address2;
	}
	public void setAddress2(String address2) {
		Address2 = address2;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public List<user_role> getUserrole() {
		return userrole;
	}
	public void setUserrole(List<user_role> userrole) {
		this.userrole = userrole;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public user_entity(Long id, @NotEmpty(message = "must me fill") String name, Gender gender, String email,
			Date createdAt, Date updatedAt, @Size(max = 10, min = 10) String phone, String address1, String address2,
			Boolean isActive, List<user_role> userrole) {
		super();
		this.id = id;
		this.name = name;
		this.gender = gender;
		this.email = email;
		CreatedAt = createdAt;
		UpdatedAt = updatedAt;
		this.phone = phone;
		Address1 = address1;
		Address2 = address2;
		this.isActive = isActive;
		this.userrole = userrole;
	}
	

}