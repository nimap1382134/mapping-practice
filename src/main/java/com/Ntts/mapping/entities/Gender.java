package com.Ntts.mapping.entities;

public enum Gender {
	
	MALE,
	FEMALE,
	OTHER;

}
